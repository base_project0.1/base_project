const webpack = require('webpack')
const common = require('./common')

const HtmlPlugin = require('html-webpack-plugin')
const DashboardPlugin = require('webpack-dashboard/plugin')

module.exports = {
  // https://webpack.js.org/configuration/devtool/
  // estilo de mapeamento do código para aprimorar o processo de debbug
  devtool: '',

  // https://webpack.js.org/concepts/entry-points/
  // arquivos que devem ser inicializados em modo de desenvolvimento
  entry: [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:3002',
    'webpack/hot/only-dev-server',
  ].concat(common.entry),

  // https://webpack.js.org/concepts/output/
  // configuração dos arquivos de saída do webpack, a configuração abaixo é a mínima,
  // apenas com o bundle .js 
  output: Object.assign({}, common.output, {
    filename: '[name].js',
  }),

  // https://webpack.js.org/concepts/plugins/
  // configuração dos plugins utilizados em modo de desenvolvimento
  plugins: [
    // https://webpack.js.org/plugins/hot-module-replacement-plugin/
    // modifica, remove e adiciona arquivos enquanto a aplicação está rodando
    new webpack.HotModuleReplacementPlugin(),

    // https://www.npmjs.com/package/webpack-dashboard
    // npm i webpack-dashboard
    // mostra a tela de compilando mais estilisada
    new DashboardPlugin(),

    // https://www.npmjs.com/package/html-webpack-plugin
    // npm i --save-dev html-webpack-plugin
    // gera o arquivo html com os arquivos do react, mesmo que nomeados por hash
    new HtmlPlugin(common.htmlPluginConfig),

    // https://webpack.js.org/plugins/ignore-plugin/
    // previne a geração dos modulos especificados
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  ],

  module: {
    rules: [
      common.jsLoader,
      common.cssLoader,
      common.stylusLoader,
      common.fileLoader,
      common.urlLoader,
    ],
  },

  resolve: common.resolve,
}
