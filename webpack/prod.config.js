const webpack = require('webpack')
const common = require('./common')

const HtmlPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CleanPlugin = require('clean-webpack-plugin')

module.exports = {
  // https://webpack.js.org/concepts/entry-points/
  // arquivos que devem ser inicializados no build
  entry: common.entry,

  // https://webpack.js.org/concepts/output/
  // configuração dos arquivos de saída do webpack no build
  output: common.output,

  plugins: [
    // https://github.com/johnagan/clean-webpack-plugin
    // npm install --save-dev clean-webpack-plugin
    // limpa todos os arquivos da pasta do build (../bin), para não juntar sujeira
    // devido ao hash nos arquivos
    new CleanPlugin([common.paths.prod], {
      root: common.paths.root,
    }),

    // https://github.com/webpack-contrib/mini-css-extract-plugin
    // npm install --save-dev mini-css-extract-plugin
    // extrai todos os .css e cria um .css por .js
    new ExtractTextPlugin({
      filename: '[name]-[hash].css',
    }),

    // https://webpack.js.org/plugins/commons-chunk-plugin/
    // compila todos os modulos utilizados em um arquivo
    new webpack.optimize.CommonsChunkPlugin({
      name: 'react-build',
      minChunks: ({ resource }) => (
        /node_modules\/react(-dom)?\//.test(resource)
      ),
    }),

    // https://www.npmjs.com/package/html-webpack-plugin
    // npm i --save-dev html-webpack-plugin
    // gera o arquivo html com os arquivos do react, mesmo que nomeados por hash
    new HtmlPlugin(common.htmlPluginConfig),

    // https://webpack.js.org/plugins/ignore-plugin/
    // previne a geração dos modulos especificados
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    
    // https://webpack.js.org/plugins/uglifyjs-webpack-plugin/
    // npm install uglifyjs-webpack-plugin --save-dev
    new webpack.optimize.UglifyJsPlugin({
      // parallel: true, // build com mult-processos rodando, aumentando a velocidade do build (bool ou number)
      // sourceMap: true, // compila as mensagens de erro do build (bool)
      mangle: true, // tipo de compressão dos arquivos
    }),

  ],

  module: {
    rules: [
      common.jsLoader,
      common.cssLoader,
      common.fileLoader,
      common.urlLoader,
      Object.assign({}, common.stylusLoader, {
        use: ExtractTextPlugin.extract({
          fallback: common.stylusLoader.use[0],
          use: common.stylusLoader.use.slice(1),
        }),
      }),
    ],
  },

  resolve: common.resolve,
}
