const { join } = require('path')

/** configuração dos caminhos comuns do projeto **/
const paths = {
  root: join(__dirname, '..'),
  dev: join(__dirname, '..', 'src'),
  prod: join(__dirname, '..', 'bin'),
  
  // Style
  cssAwesome: join(__dirname, '..', 'node_modules', 'font-awesome', 'css'),
  fontAwesome: join(__dirname, '..', 'node_modules', 'font-awesome', 'fonts'),
  reactSelect: join(__dirname, '..', 'node_modules', 'react-select', 'dist'),
  flexBoxGrid: join(__dirname, '..', 'node_modules', 'flexboxgrid', 'dist'),
  videoReact: join(__dirname, '..', 'node_modules', 'video-react'),
}
module.exports = {
  paths,

  /** Compila o código js com o babel **/
  entry: ['babel-polyfill', join(paths.dev, 'index')],

  /** Saída dos arquivos no build **/
  output: {
    path: paths.prod, // path de produção (../bin)
    filename: '[name]-[chunkhash].js', // nome do arquivo com hash
    publicPath: '', 
  },

  /** Configuração básica de do html **/
  // https://www.npmjs.com/package/html-webpack-plugin
  htmlPluginConfig: {
    title: '..:: Base Project ::..', // titulo da aba
    template: join(paths.dev, 'index.html'), // template base para inicializar o projeto
    favicon: '', // caminho do favicon
    // minify: true, //se será minificado
  },

  /** Configuração que carrega e compila os arquivos js **/
  // https://github.com/babel/babel-loader
  // npm install -D babel-loader @babel/core @babel/preset-env webpack
  jsLoader: {
    test: /\.js$/, // todos os .js
    exclude: /node_modules/, // excluindo os da pasta /node_modules/
    include: paths.dev, // incluindo apenas os .js que estiverem dentro da ./src
    use: 'babel-loader', // usar o compilador babel-loader, nos arquivos .js
  },

  /** Configuração que carrega e compila os arquivos css **/
  // obs: essa configuração é usada, pois components como cssAwesome e flexBox são em css
  cssLoader: {
    test: /\.(sa|sc|c)ss$/, // todos os .css
    include: [ // incluindo todos os caminhos que serão testados
      paths.dev,
      paths.cssAwesome,
      paths.flexBoxGrid,
      paths.reactSelect,
      paths.videoReact,
    ],
    use: [ // usando os seguintes compiladores de css
      // https://github.com/webpack-contrib/style-loader
      'style-loader', // npm install --save-dev style-loader
      // https://github.com/webpack-contrib/css-loader
      'css-loader', // npm install --save-dev css-loader
      // https://github.com/webpack-contrib/sass-loader
      'sass-loader', // npm install sass-loader node-sass webpack --save-dev
    ],
  },

  /** Configuração que carrega e compila os arquivos styl **/
  stylusLoader: {
    test: /\.styl$/, // todos os .styl
    include: paths.dev, // incluindo tudo que esteja em ..src
    use: [ // usando os seguintes compiladores de css
      // https://github.com/webpack-contrib/style-loader
      'style-loader', // npm install --save-dev style-loader
      // https://github.com/webpack-contrib/css-loader
      'css-loader?modules', // npm install --save-dev css-loader
      // https://github.com/shama/stylus-loader
      'stylus-loader', // npm install stylus-loader stylus --save-dev,
    ],
  },

  /** Configuração que carrega e compila as imagens e outros tipos de arquivos **/
  // trata os import / require() de um arquivo emitindo seu path de saída
  fileLoader: {
    test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|txt|pdf)(\?.*)?$/,
    include: [
      paths.dev,
      paths.fontAwesome,
    ],
    use: {
      // https://github.com/webpack-contrib/file-loader
      loader: 'file-loader', // npm install file-loader --save-dev
      query: {
        name: 'media/[name].[hash:8].[ext]', // arquivos serão direcionados para pasta media, nomeados com um hash
      },
    },
  },

  /** Configuração que carrega e compila os videos **/
  // transforma os arquivos em base64 URIs
  urlLoader: {
    test: /\.(mp4|webm|wav|mp3|m4a|aac|oga)(\?.*)?$/,
    include: paths.dev,
    use: {
      // https://github.com/webpack-contrib/url-loader
      loader: 'url-loader', // npm install url-loader --save-dev
      query: {
        limit: 10000,
        name: 'media/[name].[hash:8].[ext]', // arquivos serão direcionados para pasta media, nomeados com um hash
      },
    },
  },

  resolve: {
    // alias: atalhos para usar certos paths no código
    alias: {
      dev: paths.dev,
      reducers: join(paths.dev, 'reducers'),
      images: join(paths.dev, 'images'),
      theme: join(paths.dev, 'theme'),
      fonts: join(paths.dev, 'fonts'),
      components: join(paths.dev, 'components'),

      myComponents: join(paths.dev, 'components'),
      API_ComponentsBase: join(paths.dev, 'API/Components/Base'),
      API_ComponentsStyled: join(paths.dev, 'API/Components/Styled'),
      API_Structure: join(paths.dev, 'API/Structure'),
      API_Utils: join(paths.dev, 'API/Utils'),
    },
  },
}
