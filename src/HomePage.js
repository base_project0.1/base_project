//React
import React, { Component } from 'react'

//API & Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions as ThemeActions } from 'reducers/theme'

// Styles
import { ThemeProvider } from "styled-components";
import GlobalStyle from "./style";
import ClassesGlobalStyles from "theme/classes"
import FontGlobal from 'theme/fonts'

// Icons
import { FaReact } from 'react-icons/fa';
  // reference: https://react-icons.netlify.com/

// Images


// Components
import { LazyLoadImage, LazyLoadComponent } from 'react-lazy-load-image-component';
  // use to simple lazyload img tags and components
  // reference: https://www.npmjs.com/package/react-lazy-load-image-component
  // Image: <LazyLoadImage src={bgImg} />
  // Components: <LazyLoadComponent><ArticleComments id={articleId} /></LazyLoadComponent>

// Utils
import lazyload from 'API_Utils/lazyload'

class HomePage extends Component {

  componentDidMount() {
    lazyload(window)
  }

  render() {
    return (
      <ThemeProvider theme={{ name: this.props.Theme.current }}>

        <FaReact />
        Base Project Working!!!
        <FaReact />

        <FontGlobal.Faces />
        { ClassesGlobalStyles.map((ClassesGlobalStyle, index) => <ClassesGlobalStyle key={index} />) }
        <GlobalStyle />
      </ThemeProvider>
    )
    
  }
}

function mapStateToProps({ Theme }) {

  return {
    Theme
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      ...ThemeActions,
    }, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)
