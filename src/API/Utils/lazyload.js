/*

 How to use lazyload:

    --- Tag atributes ---------------------------------------------

    * class: "lazy"
    * data-src: image url
    * data-src-type: type of lazyload
    *   default: <img />
    *   bg-image: background image

    --- Example ---------------------------------------------------

    componentDidMount() {
        lazyload(window)
    }

    render() {
        <img className="lazy" data-src={bgImg} />
    }

    --- Advantage to use this lazyload -----------------------------
    
    * No load `display: none` images, even this image is inside the screen
    * Set lazyload in background images, add a atribute `data-src-type='bg-image'` 
    
    --- TODOs ------------------------------------------------------
    1- Verify element is inside horizontally
    2- Add data-src-type 'element', to make lazyload in all kind of elements (<div>, <p>, <section> ...)

*/

export default function(window){

  function loadImage (el, fn) {
      var img = new Image()
          , src = el.getAttribute('data-src');
      img.onload = function() {

          setImageByDataSourceType(el, src)

          fn? fn() : null;
      }
      img.src = src;
  }

  function setImageByDataSourceType(el, src) {
    var dataSourceType = el.getAttribute('data-src-type');

    switchImageByDataSourceType(el, el, src, dataSourceType);
  }

  function switchImageByDataSourceType(lazyEl, el, src, dataSourceType) {
      switch(dataSourceType) {
          case 'bg-image':
              el.style.backgroundImage = "url("+src+")";
              break;
          default:
              el.src = src;
      }

      ['data-src', 'data-src-type'].forEach( function(att) { lazyEl.removeAttribute(att) } )
  }

  function elementInViewport(el) {
      var rect = el.getBoundingClientRect()

      return (
          rect.top    >= 0
          && rect.left   >= 0 /* TODO: Verify element is inside horizontally */
          && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
          && getComputedStyle(el, null).display !== 'none'
      )
  }

  var images = new Array()
      , query = document.querySelectorAll('.lazy[data-src]')
      , process_lazyload = function(){

      var remainingImages = []

      for (var i = 0; i < images.length; i++) {
          if (elementInViewport(images[i])) {
              loadImage(images[i]);
              continue;
          }
          remainingImages.push(images[i])
      };

      if(images.length === remainingImages.length) return;
      
      images = remainingImages
      if(!images.length) {
          removeEventListener('scroll',process_lazyload);
          removeEventListener('resize',process_lazyload);
      }

  };

  // Array.prototype.slice.call is not callable under our lovely IE8
  for (var i = 0; i < query.length; i++) {
      images.push(query[i]);
  };

  process_lazyload();
  addEventListener('scroll',process_lazyload);
  addEventListener('resize',process_lazyload);

}