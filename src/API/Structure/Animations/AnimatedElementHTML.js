import Anim from './AnimationsSimple'

HTMLElement.prototype.myAnimations = {}
HTMLElement.prototype.StartAnimation = function(name, params, time) {
  if(typeof time !== "number") time = 1;
  this.myAnimations[name] = Anim[name](this, params, time);
}
HTMLElement.prototype.KillAnimation = function(name, params) {
  this.myAnimations[name].kill(params)
  delete this.myAnimations[name]
}
HTMLElement.prototype.KillAllAnimation = function() {
  Object.keys(this.myAnimations).forEach(animation => {
    this.myAnimations[animation].kill()
  })
  this.myAnimations = {}
}
