const initialState = {
  current: 'main'
}

const types = {
  //set types of dispatch, try keep the same name... Example:
  CHANGE_THEME: 'CHANGE_THEME',
}
export const actions = {
  //set all actions of the reducers, that be acess by another scripts... Example
  changeTheme: (theme) => (dispatch) => {
    return  dispatch({
              type: types.CHANGE_THEME,
              payload: theme,
            })
  },
}
export default (state = initialState, { type, payload }) => {
  //according with the type, return a new state
  switch (type) {
    // Example:
    case types.CHANGE_THEME:
      return {
        ...state,
        current: payload,
      }
    default:
      return state
  }
}
