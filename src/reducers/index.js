import { combineReducers } from 'redux'

//import all reducers
import Theme from './theme'

export default combineReducers({
  //set all reducers imported
  Theme ,
})
