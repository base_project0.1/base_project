import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
  body {
    overflow-x: hidden;
    
    margin: 0;

    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  
  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, "Courier New",
      monospace;
  }

  @media (min-width: 1200px) {
    .container {
        max-width: 1200px !important;
    }
  }

  img, picture, video, embed {
    max-width: 100%;
  }

  .lazy-load-image-background, .lazy-load-image-loaded, .lazy {
    display: none;
    img {
      display: none;
    }
  }

`