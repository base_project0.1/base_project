import { createGlobalStyle } from "styled-components";

import colors from "theme/colors";

// Background Class examples:
// .bg-tropicalRainForest { background-color: ${colors.tropicalRainForest}; }
// .bg-grey { background-color: ${colors.grey}; }
// .bg-vividTangerine { background-color: ${colors.vividTangerine}; }
// .bg-caramel { background-color: ${colors.caramel}; }
// .bg-coldPurple { background-color: ${colors.coldPurple}; }

export default createGlobalStyle`

`