import { createGlobalStyle } from "styled-components";

import colors from "../colors";
import font from "../fonts";

// Change the color by theme.name
// ${props => colors[props.theme.name].primary}

// Use the font by name
// ${font.names['open sans']};

export default createGlobalStyle`

* {
  font-family: ${font.names['open sans']};
  color: ${props => colors[props.theme.name].primary};
}

`
