import backgroundColors from "./backgroundColors";
import buttons from "./buttons";
import texts from "./texts";

export default [
  backgroundColors,
  buttons,
  texts,
]