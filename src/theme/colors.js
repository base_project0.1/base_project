/**
  Colors

  Obs: Mostly of the color variables names are from aproximates names.
  Reference: https://www.htmlcsscolor.com
**/

let colors = {
  black: '#000000',
  caramel: '#FCDA92',
  coldPurple: '#9C8CB9',
  gainsboro: '#E0E0E0',
  grey: '#868686',
  tropicalRainForest: '#007f56',
  vividTangerine: '#FE9481',
  whisper: '#EEEEEE',
  white: '#FFFFFF',
  
}

export default {
  ...colors,

  main: {
    primary: colors.tropicalRainForest,
    secundary: colors.grey,

    mobileMenuBg: colors.coldPurple,
    mobileMenuColor: colors.white,

    linkHover: colors.vividTangerine,
    border: colors.gainsboro,

    cardBackground: colors.white,
    cardColorTitle: colors.white,
    cardColorDescpriction: colors.grey,

    background: colors.whisper,
  },

  /* set another object with the same structure from 'main' */
  blackFriday: {
    primary: colors.tropicalRainForest,
    secundary: colors.whisper,

    mobileMenuBg: colors.tropicalRainForest,
    mobileMenuColor: colors.whisper,

    linkHover: colors.whisper,
    border: colors.whisper,

    cardBackground: colors.grey,
    cardColorTitle: colors.white,
    cardColorDescpriction: colors.white,

    background: colors.black,
  }
};
