import colors from "./colors";
import fonts from "./fonts";
import classes from "./classes";

export default {
  colors,
  fonts,
  classes,
};