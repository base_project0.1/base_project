import font_openSans from "fonts/Open_Sans/OpenSans-Regular.ttf";
import font_openSansLight from "fonts/Open_Sans/OpenSans-Light.ttf";
import font_helvetica from "fonts/helvetica-1.otf"
/*
  To add any font into the project:

  {
    name: "Name of Font",
    url: "font url imported",
  }
  
*/
export default [
  {
    name: "Open Sans",
    url: font_openSans,
  },
  {
    name: "Open Sans Light",
    url: font_openSansLight,
  },
  {
    name: "Helvetica",
    url: font_helvetica,
  },
];
